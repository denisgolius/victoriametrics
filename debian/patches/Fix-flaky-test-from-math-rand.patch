From: Aliaksandr Valialkin <valyala@victoriametrics.com>
Date: Mon, 23 Jan 2023 18:41:07 -0800
Subject: [PATCH] lib/encoding: make deterministic tests which rely on
 math/rand

Fixes https://github.com/VictoriaMetrics/VictoriaMetrics/issues/3683
Origin: upstream, commit:3d1cb011b671b71fdda8c82c2a65106b6a0dbe1e
---
 lib/encoding/compress_test.go        |  3 ++-
 lib/encoding/encoding_cgo_test.go    | 16 ++++++++++------
 lib/encoding/encoding_pure_test.go   | 16 ++++++++++------
 lib/encoding/encoding_test.go        |  6 ++++--
 lib/encoding/encoding_timing_test.go |  6 ++++--
 lib/encoding/nearest_delta2_test.go  | 10 ++++++----
 lib/encoding/nearest_delta_test.go   | 10 ++++++----
 lib/encoding/zstd/zstd_test.go       |  3 ++-
 8 files changed, 44 insertions(+), 26 deletions(-)

diff --git a/lib/encoding/compress_test.go b/lib/encoding/compress_test.go
index f959f1b..47b2d5d 100644
--- a/lib/encoding/compress_test.go
+++ b/lib/encoding/compress_test.go
@@ -9,9 +9,10 @@ func TestCompressDecompressZSTD(t *testing.T) {
 	testCompressDecompressZSTD(t, []byte("a"))
 	testCompressDecompressZSTD(t, []byte("foobarbaz"))
 
+	r := rand.New(rand.NewSource(1))
 	var b []byte
 	for i := 0; i < 64*1024; i++ {
-		b = append(b, byte(rand.Int31n(256)))
+		b = append(b, byte(r.Int31n(256)))
 	}
 	testCompressDecompressZSTD(t, b)
 }
diff --git a/lib/encoding/encoding_cgo_test.go b/lib/encoding/encoding_cgo_test.go
index ba46755..1f725ed 100644
--- a/lib/encoding/encoding_cgo_test.go
+++ b/lib/encoding/encoding_cgo_test.go
@@ -9,6 +9,8 @@ import (
 )
 
 func TestMarshalUnmarshalInt64Array(t *testing.T) {
+	r := rand.New(rand.NewSource(1))
+
 	var va []int64
 	var v int64
 
@@ -16,7 +18,7 @@ func TestMarshalUnmarshalInt64Array(t *testing.T) {
 	va = va[:0]
 	v = 0
 	for i := 0; i < 8*1024; i++ {
-		v += int64(rand.NormFloat64() * 1e6)
+		v += int64(r.NormFloat64() * 1e6)
 		va = append(va, v)
 	}
 	for precisionBits := uint8(1); precisionBits < 22; precisionBits++ {
@@ -30,7 +32,7 @@ func TestMarshalUnmarshalInt64Array(t *testing.T) {
 	va = va[:0]
 	v = 0
 	for i := 0; i < 8*1024; i++ {
-		v += 30e6 + int64(rand.NormFloat64()*1e6)
+		v += 30e6 + int64(r.NormFloat64()*1e6)
 		va = append(va, v)
 	}
 	for precisionBits := uint8(1); precisionBits < 23; precisionBits++ {
@@ -44,7 +46,7 @@ func TestMarshalUnmarshalInt64Array(t *testing.T) {
 	va = va[:0]
 	v = 1000
 	for i := 0; i < 6; i++ {
-		v += int64(rand.NormFloat64() * 100)
+		v += int64(r.NormFloat64() * 100)
 		va = append(va, v)
 	}
 	for precisionBits := uint8(1); precisionBits < 65; precisionBits++ {
@@ -55,7 +57,7 @@ func TestMarshalUnmarshalInt64Array(t *testing.T) {
 	va = va[:0]
 	v = 0
 	for i := 0; i < 6; i++ {
-		v += 3000 + int64(rand.NormFloat64()*100)
+		v += 3000 + int64(r.NormFloat64()*100)
 		va = append(va, v)
 	}
 	for precisionBits := uint8(5); precisionBits < 65; precisionBits++ {
@@ -64,11 +66,13 @@ func TestMarshalUnmarshalInt64Array(t *testing.T) {
 }
 
 func TestMarshalInt64ArraySize(t *testing.T) {
+	r := rand.New(rand.NewSource(1))
+
 	var va []int64
-	v := int64(rand.Float64() * 1e9)
+	v := int64(r.Float64() * 1e9)
 	for i := 0; i < 8*1024; i++ {
 		va = append(va, v)
-		v += 30e3 + int64(rand.NormFloat64()*1e3)
+		v += 30e3 + int64(r.NormFloat64()*1e3)
 	}
 
 	testMarshalInt64ArraySize(t, va, 1, 180, 1400)
diff --git a/lib/encoding/encoding_pure_test.go b/lib/encoding/encoding_pure_test.go
index dcb7f9f..ba3e40f 100644
--- a/lib/encoding/encoding_pure_test.go
+++ b/lib/encoding/encoding_pure_test.go
@@ -9,6 +9,8 @@ import (
 )
 
 func TestMarshalUnmarshalInt64Array(t *testing.T) {
+	r := rand.New(rand.NewSource(1))
+
 	var va []int64
 	var v int64
 
@@ -16,7 +18,7 @@ func TestMarshalUnmarshalInt64Array(t *testing.T) {
 	va = va[:0]
 	v = 0
 	for i := 0; i < 8*1024; i++ {
-		v += int64(rand.NormFloat64() * 1e6)
+		v += int64(r.NormFloat64() * 1e6)
 		va = append(va, v)
 	}
 	for precisionBits := uint8(1); precisionBits < 14; precisionBits++ {
@@ -30,7 +32,7 @@ func TestMarshalUnmarshalInt64Array(t *testing.T) {
 	va = va[:0]
 	v = 0
 	for i := 0; i < 8*1024; i++ {
-		v += 30e6 + int64(rand.NormFloat64()*1e6)
+		v += 30e6 + int64(r.NormFloat64()*1e6)
 		va = append(va, v)
 	}
 	for precisionBits := uint8(1); precisionBits < 15; precisionBits++ {
@@ -44,7 +46,7 @@ func TestMarshalUnmarshalInt64Array(t *testing.T) {
 	va = va[:0]
 	v = 1000
 	for i := 0; i < 6; i++ {
-		v += int64(rand.NormFloat64() * 100)
+		v += int64(r.NormFloat64() * 100)
 		va = append(va, v)
 	}
 	for precisionBits := uint8(1); precisionBits < 65; precisionBits++ {
@@ -55,7 +57,7 @@ func TestMarshalUnmarshalInt64Array(t *testing.T) {
 	va = va[:0]
 	v = 0
 	for i := 0; i < 6; i++ {
-		v += 3000 + int64(rand.NormFloat64()*100)
+		v += 3000 + int64(r.NormFloat64()*100)
 		va = append(va, v)
 	}
 	for precisionBits := uint8(5); precisionBits < 65; precisionBits++ {
@@ -64,11 +66,13 @@ func TestMarshalUnmarshalInt64Array(t *testing.T) {
 }
 
 func TestMarshalInt64ArraySize(t *testing.T) {
+	r := rand.New(rand.NewSource(1))
+
 	var va []int64
-	v := int64(rand.Float64() * 1e9)
+	v := int64(r.Float64() * 1e9)
 	for i := 0; i < 8*1024; i++ {
 		va = append(va, v)
-		v += 30e3 + int64(rand.NormFloat64()*1e3)
+		v += 30e3 + int64(r.NormFloat64()*1e3)
 	}
 
 	testMarshalInt64ArraySize(t, va, 1, 500, 1700)
diff --git a/lib/encoding/encoding_test.go b/lib/encoding/encoding_test.go
index fd3173a..94d25b5 100644
--- a/lib/encoding/encoding_test.go
+++ b/lib/encoding/encoding_test.go
@@ -155,12 +155,13 @@ func testMarshalUnmarshalInt64Array(t *testing.T, va []int64, precisionBits uint
 }
 
 func TestMarshalUnmarshalTimestamps(t *testing.T) {
+	r := rand.New(rand.NewSource(1))
 	const precisionBits = 3
 
 	var timestamps []int64
 	v := int64(0)
 	for i := 0; i < 8*1024; i++ {
-		v += 30e3 * int64(rand.NormFloat64()*5e2)
+		v += 30e3 * int64(r.NormFloat64()*5e2)
 		timestamps = append(timestamps, v)
 	}
 	result, mt, firstTimestamp := MarshalTimestamps(nil, timestamps, precisionBits)
@@ -174,12 +175,13 @@ func TestMarshalUnmarshalTimestamps(t *testing.T) {
 }
 
 func TestMarshalUnmarshalValues(t *testing.T) {
+	r := rand.New(rand.NewSource(1))
 	const precisionBits = 3
 
 	var values []int64
 	v := int64(0)
 	for i := 0; i < 8*1024; i++ {
-		v += int64(rand.NormFloat64() * 1e2)
+		v += int64(r.NormFloat64() * 1e2)
 		values = append(values, v)
 	}
 	result, mt, firstValue := MarshalValues(nil, values, precisionBits)
diff --git a/lib/encoding/encoding_timing_test.go b/lib/encoding/encoding_timing_test.go
index 7b100e2..fbfed7c 100644
--- a/lib/encoding/encoding_timing_test.go
+++ b/lib/encoding/encoding_timing_test.go
@@ -42,10 +42,11 @@ func BenchmarkUnmarshalGaugeArray(b *testing.B) {
 }
 
 var benchGaugeArray = func() []int64 {
+	r := rand.New(rand.NewSource(1))
 	a := make([]int64, 8*1024)
 	v := int64(0)
 	for i := 0; i < len(a); i++ {
-		v += int64(rand.NormFloat64() * 100)
+		v += int64(r.NormFloat64() * 100)
 		a[i] = v
 	}
 	return a
@@ -230,10 +231,11 @@ var benchMarshalType = func() MarshalType {
 }()
 
 var benchInt64Array = func() []int64 {
+	r := rand.New(rand.NewSource(1))
 	var a []int64
 	var v int64
 	for i := 0; i < 8*1024; i++ {
-		v += 30e3 + int64(rand.NormFloat64()*1e3)
+		v += 30e3 + int64(r.NormFloat64()*1e3)
 		a = append(a, v)
 	}
 	return a
diff --git a/lib/encoding/nearest_delta2_test.go b/lib/encoding/nearest_delta2_test.go
index dcf0520..7a4586b 100644
--- a/lib/encoding/nearest_delta2_test.go
+++ b/lib/encoding/nearest_delta2_test.go
@@ -146,6 +146,8 @@ func testMarshalInt64NearestDelta2(t *testing.T, va []int64, precisionBits uint8
 }
 
 func TestMarshalUnmarshalInt64NearestDelta2(t *testing.T) {
+	r := rand.New(rand.NewSource(1))
+
 	testMarshalUnmarshalInt64NearestDelta2(t, []int64{0, 0}, 4)
 	testMarshalUnmarshalInt64NearestDelta2(t, []int64{1, -3}, 4)
 	testMarshalUnmarshalInt64NearestDelta2(t, []int64{255, 255}, 4)
@@ -198,7 +200,7 @@ func TestMarshalUnmarshalInt64NearestDelta2(t *testing.T) {
 	v = 787933
 	va = []int64{}
 	for i := 0; i < 1024; i++ {
-		v -= 25 + int64(rand.NormFloat64()*2)
+		v -= 25 + int64(r.NormFloat64()*2)
 		va = append(va, v)
 	}
 	testMarshalUnmarshalInt64NearestDelta2(t, va, 4)
@@ -207,7 +209,7 @@ func TestMarshalUnmarshalInt64NearestDelta2(t *testing.T) {
 	v = 943854
 	va = []int64{}
 	for i := 0; i < 1024; i++ {
-		v += 30 + rand.Int63n(5)
+		v += 30 + r.Int63n(5)
 		va = append(va, v)
 	}
 	testMarshalUnmarshalInt64NearestDelta2(t, va, 4)
@@ -216,7 +218,7 @@ func TestMarshalUnmarshalInt64NearestDelta2(t *testing.T) {
 	v = -12345
 	va = []int64{}
 	for i := 0; i < 1024; i++ {
-		v += int64(rand.NormFloat64() * 10)
+		v += int64(r.NormFloat64() * 10)
 		va = append(va, v)
 	}
 	testMarshalUnmarshalInt64NearestDelta2(t, va, 2)
@@ -225,7 +227,7 @@ func TestMarshalUnmarshalInt64NearestDelta2(t *testing.T) {
 	v = -12345
 	va = []int64{}
 	for i := 0; i < 1024; i++ {
-		v += rand.Int63n(15) - 1
+		v += r.Int63n(15) - 1
 		va = append(va, v)
 	}
 	testMarshalUnmarshalInt64NearestDelta2(t, va, 3)
diff --git a/lib/encoding/nearest_delta_test.go b/lib/encoding/nearest_delta_test.go
index fd45720..7a12209 100644
--- a/lib/encoding/nearest_delta_test.go
+++ b/lib/encoding/nearest_delta_test.go
@@ -53,6 +53,8 @@ func testMarshalInt64NearestDelta(t *testing.T, va []int64, precisionBits uint8,
 }
 
 func TestMarshalUnmarshalInt64NearestDelta(t *testing.T) {
+	r := rand.New(rand.NewSource(1))
+
 	testMarshalUnmarshalInt64NearestDelta(t, []int64{0}, 4)
 	testMarshalUnmarshalInt64NearestDelta(t, []int64{0, 0}, 4)
 	testMarshalUnmarshalInt64NearestDelta(t, []int64{1, -3}, 4)
@@ -105,7 +107,7 @@ func TestMarshalUnmarshalInt64NearestDelta(t *testing.T) {
 	v = 787933
 	va = []int64{}
 	for i := 0; i < 1024; i++ {
-		v -= 25 + int64(rand.NormFloat64()*2)
+		v -= 25 + int64(r.NormFloat64()*2)
 		va = append(va, v)
 	}
 	testMarshalUnmarshalInt64NearestDelta(t, va, 4)
@@ -114,7 +116,7 @@ func TestMarshalUnmarshalInt64NearestDelta(t *testing.T) {
 	v = 943854
 	va = []int64{}
 	for i := 0; i < 1024; i++ {
-		v += 30 + rand.Int63n(5)
+		v += 30 + r.Int63n(5)
 		va = append(va, v)
 	}
 	testMarshalUnmarshalInt64NearestDelta(t, va, 4)
@@ -123,7 +125,7 @@ func TestMarshalUnmarshalInt64NearestDelta(t *testing.T) {
 	v = -12345
 	va = []int64{}
 	for i := 0; i < 1024; i++ {
-		v += int64(rand.NormFloat64() * 10)
+		v += int64(r.NormFloat64() * 10)
 		va = append(va, v)
 	}
 	testMarshalUnmarshalInt64NearestDelta(t, va, 4)
@@ -132,7 +134,7 @@ func TestMarshalUnmarshalInt64NearestDelta(t *testing.T) {
 	v = -12345
 	va = []int64{}
 	for i := 0; i < 1024; i++ {
-		v += rand.Int63n(15) - 1
+		v += r.Int63n(15) - 1
 		va = append(va, v)
 	}
 	testMarshalUnmarshalInt64NearestDelta(t, va, 4)
diff --git a/lib/encoding/zstd/zstd_test.go b/lib/encoding/zstd/zstd_test.go
index 6f7f63b..124b60e 100644
--- a/lib/encoding/zstd/zstd_test.go
+++ b/lib/encoding/zstd/zstd_test.go
@@ -15,9 +15,10 @@ func TestCompressDecompress(t *testing.T) {
 	testCrossCompressDecompress(t, []byte("a"))
 	testCrossCompressDecompress(t, []byte("foobarbaz"))
 
+	r := rand.New(rand.NewSource(1))
 	var b []byte
 	for i := 0; i < 64*1024; i++ {
-		b = append(b, byte(rand.Int31n(256)))
+		b = append(b, byte(r.Int31n(256)))
 	}
 	testCrossCompressDecompress(t, b)
 }
